# globallogic-ms-user

Es un crud de usuarios para la creación, actualización, login, lectura y borrado lógico de usuarios. 

## Installation
1.- Clone el proyecto desde la siguiente URL https://gitlab.com/carelys23/globallogic-ms-user.git.

2.- Importelo como un proyecto gradle en el ide de su preferencia. *Para esta prueba se utilizó STS.

3.- Para ejecutar la aplicación, presionar boton derecho a la clase UserApplication.java, seleccionar la opción run as -> spring boot app. La aplicación levanta en el puerto 8080 en caso querer usar otro puerto, cambiarlo en src/main/resources/application.yml


## Cambiar puerto:

```python
server:
  port: 8080 //puerto que quiera usar
```