package com.globallogic.user.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.globallogic.user.dto.UserCreated;
import com.globallogic.user.model.User;

public interface UserService {
	
	public ResponseEntity<?> login(User user);

	public List<User> getAllUser();

	public User getOneUser(String email);

	public User updateUser(User user);

	public UserCreated createUser(User user);

	public User deleteUser(User user);

}
