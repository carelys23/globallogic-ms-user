package com.globallogic.user.service;

public interface TokenService {
	
	public String getToken(String name);

}
