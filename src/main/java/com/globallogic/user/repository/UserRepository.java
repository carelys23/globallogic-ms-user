package com.globallogic.user.repository;


import org.springframework.stereotype.Repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.globallogic.user.model.User;

@Repository
public interface UserRepository extends ArangoRepository<User, String> {

	public User findByEmail(String email);


}
