package com.globallogic.user.config;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDB.Builder;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;

@Configuration
@EnableArangoRepositories(basePackages = { "com.globallogic.user.repository" })
public class ArangoConfig extends AbstractArangoConfiguration {

	@Value("${spring.data.arangodb.host}")
	private String host;
	@Value("${spring.data.arangodb.port}")
	private int port;
	@Value("${spring.data.arangodb.database}")
	private String databaseName;
	@Value("${spring.data.arangodb.username}")
	private String userName;
	@Value("${spring.data.arangodb.password}")
	private String password;
	@Value("${spring.data.arangodb.encoded}")
	private String encoded;
	@Value("${spring.data.arangodb.certificateFactory}")
	private String certificateFactory;
	@Value("${spring.data.arangodb.certificateEntry}")
	private String certificateEntry;
	@Value("${spring.data.arangodb.ssl}")
	private String ssl;

	@Override
	public Builder arango() {
		InputStream is = new java.io.ByteArrayInputStream(Base64.getDecoder().decode(encoded));
		SSLContext sslContext;
		try {
			CertificateFactory cf = CertificateFactory.getInstance(certificateFactory);
			X509Certificate caCert = (X509Certificate) cf.generateCertificate(is);
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
			ks.load(null);
			ks.setCertificateEntry(certificateEntry, caCert);
			tmf.init(ks);
			sslContext = SSLContext.getInstance(ssl);
			sslContext.init(null, tmf.getTrustManagers(), null);
		} catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException
				| KeyManagementException e) {
			throw new RuntimeException(e);
		}
		return new ArangoDB.Builder().useSsl(true).host(host, port).user(userName).password(password)
				.sslContext(sslContext);

	}

	@Override
	public String database() {
		return databaseName;
	}

}
