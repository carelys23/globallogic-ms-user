package com.globallogic.user.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ConstantUtil {
	
	public static final String USUARIO = " Usuario: ";
	public static final String USUARIO_NO_REGISTRADO = " Usuario no registrado";
	public static final String USUARIOS_NO_REGISTRADOS = "No se encontraron usuarios registrados";
	public static final String CORREO_YA_REGISTRADO = " Correo ya registrado";
	public static final String CREADO = " creado";
	public static final String CORREO_INVALIDO = "correo inválido";
	public static final String CANTIDAD_USUARIOS = " Cantidad de usuarios encontrados: ";
	public static final String LOGIN_EXITOSO = " login exitoso";
	public static final String AUTENTICACION_INVALIDA = " Autenticación inválida";

}
