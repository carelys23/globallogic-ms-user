package com.globallogic.user.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidateUtil {

	public static boolean validateEmail(String email) {
		if (email == null || email.isEmpty()) {
			return false;
		}

		if (!email.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,4})$")) {
			return false;
		}
		return true;
	}

}
