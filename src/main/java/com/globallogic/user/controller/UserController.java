package com.globallogic.user.controller;

import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.globallogic.user.dto.ResponseRequest;
import com.globallogic.user.dto.UserCreated;
import com.globallogic.user.model.User;
import com.globallogic.user.service.UserService;
import com.globallogic.user.util.ConstantUtil;
import com.globallogic.user.util.ValidateUtil;

@RestController
@RequestMapping("/")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping(value = "/login", consumes = { MediaType.APPLICATION_JSON }, produces = { MediaType.APPLICATION_JSON })
	public ResponseEntity<?> saveUser(@RequestBody User user) {
		if (!ValidateUtil.validateEmail(user.getEmail())) {
			return new ResponseEntity<>(new ResponseRequest(HttpStatus.CONFLICT.value(), ConstantUtil.CORREO_INVALIDO),
					HttpStatus.CONFLICT);
		}
		return userService.login(user);

	}

	@PostMapping(value = "/user", consumes = { MediaType.APPLICATION_JSON }, produces = { MediaType.APPLICATION_JSON })
	public ResponseEntity<?> createUser(@RequestBody User user) {
		if (!ValidateUtil.validateEmail(user.getEmail())) {
			return new ResponseEntity<>(new ResponseRequest(HttpStatus.CONFLICT.value(), ConstantUtil.CORREO_INVALIDO),
					HttpStatus.CONFLICT);
		}
		UserCreated create = userService.createUser(user);
		if (create != null) {
			return new ResponseEntity<>(create, HttpStatus.CREATED);
		}
		return new ResponseEntity<>(new ResponseRequest(HttpStatus.BAD_REQUEST.value(), ConstantUtil.CORREO_YA_REGISTRADO),
				HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/user", produces = { MediaType.APPLICATION_JSON })
	public ResponseEntity<?> getAllUser() {
		List<User> listUser = userService.getAllUser();
		if (listUser.isEmpty()) {
			return new ResponseEntity<>(
					new ResponseRequest(HttpStatus.NOT_FOUND.value(), ConstantUtil.USUARIOS_NO_REGISTRADOS),
					HttpStatus.NOT_FOUND);
		}
		listUser.forEach(p -> p.setId(UUID.randomUUID().toString()));
		return new ResponseEntity<>(listUser, HttpStatus.OK);
	}

	@GetMapping(value = "/user/{email}", produces = { MediaType.APPLICATION_JSON })
	public ResponseEntity<?> getOneUser(@RequestParam(required = true) String email) {
		User user = userService.getOneUser(email);
		if (user == null) {
			return new ResponseEntity<>(new ResponseRequest(HttpStatus.NOT_FOUND.value(), ConstantUtil.USUARIO_NO_REGISTRADO),
					HttpStatus.NOT_FOUND);
		}
		user.setId(UUID.randomUUID().toString());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@PutMapping(value = "/user", consumes = { MediaType.APPLICATION_JSON }, produces = { MediaType.APPLICATION_JSON })
	public ResponseEntity<?> getupdateUser(@RequestBody User user) {
		User userDB = userService.updateUser(user);
		if (userDB == null) {
			return new ResponseEntity<>(new ResponseRequest(HttpStatus.NOT_FOUND.value(), ConstantUtil.USUARIO_NO_REGISTRADO),
					HttpStatus.NOT_FOUND);
		}
		userDB.setId(UUID.randomUUID().toString());
		return new ResponseEntity<>(userDB, HttpStatus.OK);
	}

	@DeleteMapping(value = "/user", consumes = { MediaType.APPLICATION_JSON }, produces = {
			MediaType.APPLICATION_JSON })
	public ResponseEntity<?> deleteUser(@RequestBody User user) {
		User userDB = userService.deleteUser(user);
		if (userDB == null) {
			return new ResponseEntity<>(new ResponseRequest(HttpStatus.NOT_FOUND.value(), ConstantUtil.USUARIO_NO_REGISTRADO),
					HttpStatus.NOT_FOUND);
		}
		userDB.setId(UUID.randomUUID().toString());
		return new ResponseEntity<>(userDB, HttpStatus.OK);
	}

}
