package com.globallogic.user.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.globallogic.user.dto.ResponseRequest;
import com.globallogic.user.dto.UserCreated;
import com.globallogic.user.model.User;
import com.globallogic.user.repository.UserRepository;
import com.globallogic.user.service.TokenService;
import com.globallogic.user.service.UserService;
import com.globallogic.user.util.ConstantUtil;

@Service("userService")
public class UserImpl implements UserService {

	private static final Logger logger = Logger.getLogger(UserImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TokenService tokenService;

	@Override
	public ResponseEntity<?> login(User user) {
		User userDB = userRepository.findByEmail(user.getEmail());

		if (userDB == null) {
			logger.warn(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.USUARIO_NO_REGISTRADO);
			return new ResponseEntity<>(
					new ResponseRequest(HttpStatus.BAD_REQUEST.value(), ConstantUtil.USUARIO_NO_REGISTRADO),
					HttpStatus.BAD_REQUEST);
		}

		if (!user.getPassword().equalsIgnoreCase(userDB.getPassword())) {
			logger.warn(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.AUTENTICACION_INVALIDA);
			return new ResponseEntity<>(
					new ResponseRequest(HttpStatus.BAD_REQUEST.value(), ConstantUtil.AUTENTICACION_INVALIDA),
					HttpStatus.BAD_REQUEST);
		}

		updateLastLogin(userDB);
		logger.info(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.LOGIN_EXITOSO);
		return new ResponseEntity<>(getUserCreate(userDB), HttpStatus.OK);

	}

	private UserCreated getUserCreate(User user) {
		UserCreated userCreate = new UserCreated();
		userCreate.setId(UUID.randomUUID().toString());
		userCreate.setName(user.getName());
		userCreate.setPhones(user.getPhones());
		userCreate.setEmail(user.getEmail());
		userCreate.setCreated(user.getCreatedDate());
		userCreate.setModified(user.getModifieddDate());
		userCreate.setLastLogin(user.getLastLogin());
		userCreate.setToken(user.getToken());
		userCreate.setIsactive(user.isActive());
		return userCreate;
	}

	private void updateLastLogin(User user) {
		user.setLastLogin(new Date());
		user.setToken(tokenService.getToken(user.getName()));
		userRepository.save(user);

	}

	private void createtUser(User user) {
		user.setCreatedDate(new Date());
		user.setModifieddDate(new Date());
		user.setLastLogin(new Date());
		user.setToken(tokenService.getToken(user.getName()));
		userRepository.save(user);
	}

	@Override
	public UserCreated createUser(User user) {
		User userDB = userRepository.findByEmail(user.getEmail());
		if (userDB == null) {
			createtUser(user);
			logger.info(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.CREADO);
			return getUserCreate(user);
		}
		logger.warn(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.CORREO_YA_REGISTRADO);
		return null;
	}

	@Override
	public List<User> getAllUser() {
		List<User> listUser = new ArrayList<>();
		userRepository.findAll().forEach(listUser::add);
		logger.info(ConstantUtil.CANTIDAD_USUARIOS + listUser.size());
		return listUser;

	}

	@Override
	public User getOneUser(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User updateUser(User user) {
		User userDB = userRepository.findByEmail(user.getEmail());
		if (userDB == null) {
			logger.warn(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.USUARIO_NO_REGISTRADO);
			return userDB;
		}
		userDB.setModifieddDate(new Date());
		userDB.setEmail(user.getEmail());
		userDB.setName(user.getName());
		userDB.setPassword(user.getPassword());
		userDB.setPhones(user.getPhones());
		return userRepository.save(userDB);
	}

	@Override
	public User deleteUser(User user) {
		User userDB = userRepository.findByEmail(user.getEmail());
		if (userDB == null) {
			logger.warn(ConstantUtil.USUARIO + user.getEmail() + ConstantUtil.USUARIO_NO_REGISTRADO);
			return null;
		}
		userDB.setActive(false);
		return userRepository.save(userDB);

	}

}
