package com.globallogic.user.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.globallogic.user.dto.UserCreated;
import com.globallogic.user.model.User;
import com.globallogic.user.service.UserService;
import com.globallogic.user.util.JsonUtil;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	private UserController userController;

	@Mock
	private UserService userService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}
	

	@Test
	public void createUserTest() throws Exception {
		HttpHeaders httpHeaders = getHeader();
		
		User user= getObjectDTO();
		user.setEmail("pruebagmail.com");
		this.mockMvc
				.perform(post("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(user)))
				.andDo(print()).andExpect(status().isConflict());
		
		when(userService.createUser(any())).thenReturn(null);
		this.mockMvc
				.perform(post("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(getObjectDTO())))
				.andDo(print()).andExpect(status().isBadRequest());
		when(userService.createUser(any())).thenReturn(getObjectCreatedDTO());
		this.mockMvc
				.perform(post("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(getObjectDTO())))
				.andDo(print()).andExpect(status().isCreated());
	}

	@Test
	public void getAllUserTest() throws Exception {
		HttpHeaders httpHeaders = getHeader();
		List<User> list = new ArrayList<>();
		list.add(getObjectDTO());
		when(userService.getAllUser()).thenReturn(list);
		this.mockMvc.perform(get("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk());

		when(userService.getAllUser()).thenReturn(new ArrayList<>());
		this.mockMvc.perform(get("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isNoContent());
	}

	@Test
	public void getupdateUserTest() throws Exception {
		HttpHeaders httpHeaders = getHeader();
		when(userService.updateUser(getObjectDTO())).thenReturn(null);
		this.mockMvc
				.perform(put("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(getObjectDTO())))
				.andDo(print()).andExpect(status().isNotFound());
		when(userService.updateUser(any())).thenReturn(getObjectDTO());
		this.mockMvc
				.perform(put("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(getObjectDTO())))
				.andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void deleteUserTest() throws Exception {
		HttpHeaders httpHeaders = getHeader();
		when(userService.deleteUser(getObjectDTO())).thenReturn(null);
		this.mockMvc
				.perform(delete("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(getObjectDTO())))
				.andDo(print()).andExpect(status().isNotFound());
		when(userService.deleteUser(any())).thenReturn(getObjectDTO());
		this.mockMvc
				.perform(delete("/user").headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(JsonUtil.objectToJson(getObjectDTO())))
				.andDo(print()).andExpect(status().isOk());
	}

	private User getObjectDTO() {
		User user = new User();
		user.setCreatedDate(new Date());
		user.setEmail("test@gmail.com");
		user.setLastLogin(new Date());
		user.setModifieddDate(new Date());
		user.setName("Diana");
		user.setPassword("123456");
		user.setToken("abdc");
		return user;
	}
	
	private UserCreated getObjectCreatedDTO() {
		UserCreated user = new UserCreated();
		user.setCreated(new Date());
		user.setEmail("test@gmail.com");
		user.setLastLogin(new Date());
		user.setModified(new Date());
		user.setName("Diana");
		user.setToken("abdc");
		return user;
	}

	private HttpHeaders getHeader() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Content-Type", "application/json");
		httpHeaders.add("Authorization", getToken("diana"));
		return httpHeaders;
	}
	

	public String getToken(String name) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId("softtekJWT").setSubject(name)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 6000000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;

	}

}
